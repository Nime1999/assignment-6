#include <stdio.h>
#include <stdlib.h>

void Pattern(int a);
void Row(int b);
int NoofRows=1;
int r;

void Pattern(int a)
{

    if(a>0)
    {
        Row(NoofRows);
        printf("\n");
        NoofRows++;
        Pattern(a-1);
    }
}
void Row(int b)
{
    if(b>0)
    {
        printf("%d",b);
        Row(b-1);
    }
}
int main()
{
    printf("Enter the Number of Rows : ");
    scanf("%d",&r);
    Pattern(r);
    return 0;
}
